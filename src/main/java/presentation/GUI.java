package presentation;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import dao.OrderDAO;
import model.Order;

public class GUI {

	public GUI() {
		ClientBLL clientBLL = new ClientBLL();
		ProductBLL prodBLL = new ProductBLL();
		OrderBLL orderBLL = new OrderBLL();
		Controller ctrl = new Controller();
		
		
		JFrame frame = new JFrame("Warehouse");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		frame.setVisible(true);
		
		JPanel container = new JPanel();
		//JPanel main = new JPanel();
		//main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		CardLayout cl = new CardLayout();
		container.setLayout(cl);
		frame.add(container);
		
		JButton selectCustScrn = new JButton("Customers");
		JButton selectOrdScrn = new JButton("Orders");
		JButton selectProdScrn = new JButton("Products");
		
		JPanel first = new JPanel();
		JPanel p1 = new JPanel();
		container.add(first, "1");
		first.add(p1);
		p1.add(selectCustScrn);
		selectCustScrn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame2 = new JFrame("Clients");
				JPanel container2 = new JPanel();
				frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame2.setSize(350, 400);
				frame2.setVisible(true);
				frame2.add(container2);
				JTable custTable = ctrl.createTable(clientBLL.getClientList());
				container2.add(custTable);
				
				JButton adder = new JButton("Add");
				JButton remover = new JButton("Remove");
				JButton updater = new JButton("Update");
				
				container2.add(adder);
				container2.add(remover);
				container2.add(updater);
				//frame2.pack();
				
				adder.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						//TODO: clientBLL.insertClient()
						Form form = new Form("id", "name", "address", "email");
						form.getButton().addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								clientBLL.insertClient(Integer.parseInt(form.getVal1()), form.getVal2(), form.getVal3(), form.getVal4());
							}
						});
					}
				});
				
				remover.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						Form form = new Form("id", "name", "address", "email");
						form.getButton().addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								clientBLL.deleteClient(Integer.parseInt(form.getVal1()));
							}
						});
					}
				});
				
				updater.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						Form form = new Form("id", "name", "address", "email");
						form.getButton().addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								clientBLL.updateClient(Integer.parseInt(form.getVal1()), form.getVal2(), form.getVal3(), form.getVal4());
							}
						});
					}
				});
			}
		});
		
		JPanel p2 = new JPanel();
		first.add(p2);
		p2.add(selectProdScrn);
		selectProdScrn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame3 = new JFrame("Products");
				JPanel container3 = new JPanel();
				frame3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame3.setSize(350, 400);
				frame3.setVisible(true);
				frame3.add(container3);
				JTable prodTable = ctrl.createTable(prodBLL.getProductList());
				container3.add(prodTable);
				
				JButton adder = new JButton("Add");
				JButton remover = new JButton("Remove");
				JButton updater = new JButton("Update");
				
				container3.add(adder);
				container3.add(remover);
				container3.add(updater);
				//frame3.pack();
				
				adder.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
			
						Form form = new Form("id", "name", "price", "quantity");
						form.getButton().addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								prodBLL.insertProduct(Integer.parseInt(form.getVal1()), form.getVal2(), Integer.parseInt(form.getVal3()), Integer.parseInt(form.getVal4()));
							}
						});
					}
				});
				
				remover.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						Form form = new Form("id", "name", "price", "quantity");
						form.getButton().addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								prodBLL.deleteProduct(Integer.parseInt(form.getVal1()));
							}
						});
					}
				});
				
				updater.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						Form form = new Form("id", "name", "price", "quantity");
						form.getButton().addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								prodBLL.updateProduct(Integer.parseInt(form.getVal1()), form.getVal2(), Integer.parseInt(form.getVal3()), Integer.parseInt(form.getVal4()));
							}
						});
					}
				});
			}
	});
		
		JPanel p3 = new JPanel();
		first.add(p3);
		p3.add(selectOrdScrn);
		selectOrdScrn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame4 = new JFrame("Orders");
				JPanel container4 = new JPanel();
				frame4.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame4.setSize(350, 400);
				frame4.setVisible(true);
				frame4.add(container4);
				JTable ordTable = ctrl.createTable(orderBLL.getOrderList());
				container4.add(ordTable);
				
				JTextField cust = new JTextField("customerID");
				JTextField prod = new JTextField("productID");
				JTextField quantity = new JTextField("quantity");
				
				container4.add(cust);
				container4.add(prod);
				container4.add(quantity);
				
				JButton go = new JButton("Create");
				container4.add(go);
				
				go.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						orderBLL.insert(new Order(getLastId()+1, Integer.valueOf(cust.getText()), Integer.valueOf(prod.getText()), Integer.valueOf(quantity.getText())));
					}
				});
			}
	});
		JTextField idField = new JTextField("Client ID");
		JButton starter = new JButton("Receipt");
		first.add(idField);
		first.add(starter);
		
		JPanel second = new JPanel();
		JTextArea receipt = new JTextArea(5, 30);
		container.add(second, "2");
		second.add(receipt);
		
		starter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cl.show(container, "2");
				if(clientBLL.printReceipt(Integer.parseInt(idField.getText())).equals(""))
					receipt.setText("Receipt is empty.");
				else
					receipt.setText(clientBLL.printReceipt(Integer.parseInt(idField.getText())));
			}
	});
		
		JButton returner = new JButton("Return");
		second.add(returner);
		
		returner.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cl.show(container,  "1");
				receipt.setText("");
			}
		});
		
		
		frame.pack();
		
		
}
	public int getLastId() {
		int lastId;
		OrderDAO orderDAO = new OrderDAO();
		List<Order> orders = orderDAO.findAll();
		lastId = orders.get(orders.size()-1).getId();
		return lastId;
	}
}
