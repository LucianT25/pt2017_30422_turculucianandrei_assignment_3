package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.Order;

public class OrderDAO extends AbstractDAO<Order>{
	public List<Order> receipt(int id) {
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			String query = "SELECT * FROM warehousedb.order WHERE clientID = '" + id +"';";
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(query);
				resultSet = statement.executeQuery();

				return createObjects(resultSet);
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, Order.class.getName() + "DAO:findById " + e.getMessage());
			} catch(IndexOutOfBoundsException e) {
				JOptionPane.showMessageDialog(null, "Data not found.", "Error", 0); 
			} finally {
				ConnectionFactory.close(resultSet);
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
	}
}
