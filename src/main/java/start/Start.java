package start;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import presentation.GUI;


public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new GUI();
			}
		});
	}
}

