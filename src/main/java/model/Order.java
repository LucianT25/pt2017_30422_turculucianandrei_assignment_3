package model;

public class Order {
	private int id;
	private int clientID;
	private int productID;
	private int quantity;
	
	public Order() {
	}
	
	public Order(int id, int clientid, int productid, int quantity) {
		super();
		this.id = id;
		this.clientID = clientid;
		this.productID = productid;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", clientID=" + clientID + ", productID=" + productID + ", quantity=" + quantity
				+ "]";
	}
	
	public String toInsert() {
		return "("+getId()+","+getClientID()+", "+getProductID()+","+getQuantity()+")";
	}
	
}
