package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JOptionPane;

import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import dao.OrderDAO;
import model.Client;
import model.Order;

public class ClientBLL {

	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;
	private OrderDAO orderDAO;
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());

		clientDAO = new ClientDAO();
		orderDAO = new OrderDAO();
	}

	public Client findClientById(int id) {
		Client cl = clientDAO.findById(id);
		if (cl == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return cl;
	}
	
	public List<Client> getClientList() {
		List<Client> clientList = clientDAO.findAll();
		if (clientList == null) {
			throw new NoSuchElementException("There are no clients in the table");
		}
		return clientList;
	}
	
	public String printReceipt(int id) {
		List<Order> orders = orderDAO.receipt(id);
		StringBuilder sb = new StringBuilder();
		if(orders.isEmpty())
			JOptionPane.showMessageDialog(null, "Data not found.", "Error", 0); 
		for(Order o: orders) {
			sb.append(o.toString()+"\n");
		}
		return sb.toString();
	}
	
	public void insertClient(int id, String name, String address, String email) {
		Client cl = new Client(id, name, address, email);
		clientDAO.insert(cl);
		return;
	}
	
	public void deleteClient(int id) {
		if(findClientById(id) != null);
			clientDAO.delete(id);
		return;
	}
	
	public void updateClient(int id, String name, String address, String email) {
		Client cl = new Client(id, name, address, email);
		clientDAO.update(cl);
	}
}
