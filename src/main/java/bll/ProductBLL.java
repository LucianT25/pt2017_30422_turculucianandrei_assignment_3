package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	ProductDAO productDAO;
	
	public ProductBLL() {
		this.productDAO = new ProductDAO();
	}
	
	public List<Product> getProductList() {
		List<Product> productList = productDAO.findAll();
		if (productList == null) {
			throw new NoSuchElementException("There are no products in the table");
		}
		return productList;
	}
	
	public Product findProductById(int id) {
		Product p = productDAO.findById(id);
		if (p == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return p;
	}
	
	public void insertProduct(int id, String name, int price, int quantity) {
		Product p = new Product(id, name, price, quantity);
		productDAO.insert(p);
		return;
	}
	
	public void deleteProduct(int id) {
		if(findProductById(id) != null);
			productDAO.delete(id);
		return;
	}
	
	public void updateProduct(int id, String name, int price, int quantity) {
		Product p = new Product(id, name, price, quantity);
		productDAO.update(p);
	} 
}
