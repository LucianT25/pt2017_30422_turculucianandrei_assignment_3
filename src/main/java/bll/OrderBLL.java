package bll;

import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JOptionPane;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

public class OrderBLL {
	private OrderDAO orderDAO;
	
	public OrderBLL() {
		orderDAO = new OrderDAO();
	}
	
	public List<Order> getOrderList() {
		List<Order> orderList = orderDAO.findAll();
		if (orderList == null) {
			throw new NoSuchElementException("There are no orders in the table");
		}
		return orderList;
	}
	
	public Order findById(int id) {
		Order order = orderDAO.findById(id);
		if (order == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return order;
	}
	
	public void insert(Order o) {
		if(o.getQuantity() < 0) {
			JOptionPane.showMessageDialog(null, "invalid quantity", "Error", 0);
			return;
		}
		ClientDAO cDAO = new ClientDAO();
		Client c = cDAO.findById(o.getClientID());
		
		if (c == null) {
			throw new NoSuchElementException("The client with id =" + o.getClientID() + " was not found!");
		}
		
		ProductDAO pDAO = new ProductDAO();
		Product p = pDAO.findById(o.getProductID());
		
		if(p.getQuantity() < o.getQuantity()) {
			JOptionPane.showMessageDialog(null, "stock too low", "Error", 0);
			return;
		}
			
		p.setQuantity(p.getQuantity() - o.getQuantity());
		
		orderDAO.insert(o);
		pDAO.update(p);
	}
}
